@registration @smoke
Feature: Registration Form

  As a user I will be able to create a new account

	Background:
		When Open "REGISTRATION" form
		Then Check modal window is present

	Scenario: Create a new account (for  user less 13 age)		
		When Click "Less 13 age" button
		Then Check message "Sorry, you can’t register the child because you’re outside the UK" is present
		When Click "OK" button
		Then Check "HOME" page is present


  Scenario: Create a new account (for user more 13 age)		
		When Click "More 13 age" button
		And Type value in "DAY OF BIRTHDAY" field
		And Type value in "MONTH OF BIRTHDAY" field
		And Type value in "YEAR OF BIRTHDAY" field
		And Click "CONTINUE" button
		And Type value in "EMAIL" field
		And Type value in "PASSWORD" field
		And Select "Christmas Island" in "COUNTRY" field
		And Click "Register" button
		Then Check message "OK you’re signed in. Now, want to keep up to date?" is present