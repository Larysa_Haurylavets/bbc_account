'use strict';

const EC = protractor.ExpectedConditions,
  dateFormat = require('dateformat');

class Helper{

  waitAndClick = element => {
    return this.waitForElementToBeClikable(element)
      .then(() => {
        return element.click();
      });
  };

  waitAndSendKeys = (element, text) => {
    return this.waitForElementAppearance(element)
      .then(() => {
        return element.clear().sendKeys(text);
      });
  };

  waitVisibility = element => browser.wait(EC.visibilityOf(element));

  formatDate = (date, mask) => dateFormat(new Date(date), mask);

  formatDay = (day) => {
    mapDay = new Map([
      [0, 'Sunday'],
      [1, 'Monday'],
      [2, 'Tuesday'],
      [3, 'Wednesday'],
      [4, 'Thursday'],
      [5, 'Friday'],
      [6, 'Saturday']
    ]);

    return mapDay.get(day);
  }

  navigateBack = () => browser.navigate().back();

  scrollTop = () => browser.executeScript("window.scrollTo(0,0);");

  javaScriptClick = element => browser.executeScript('arguments[0].click()', element.getWebElement());

  waitForElementAppearance = element => browser.driver.wait(EC.presenceOf(element, browser.elementTimeout));

  waitForElementToBeClikable = element => browser.wait(EC.elementToBeClickable(element), 7000, 'Element not clickable');

  scrollAndClick = element => {
    return this.scrollToElement(element)
      .then(() => {
        return element.click();
      }).then(() => {
        return this.switchToShortMode();
      });
  };

  scrollAndSendKeys = (element, text) => {
    return this.scrollToElement(element)
      .then(() => {
        element.clear().sendKeys(text);
      });
  };

}

module.exports = Helper;