'use strict';

const { element } = require('protractor'),
  BasicPage = require('./basicPage'),
  format = require('string-format');

class RegistrationPage extends BasicPage{
  constructor(){
    this.dayInputField = element(by.name('dateOfBirthDay'));
    this.monthInputField = element(by.name('dateOfBirthMonth'));
    this.yearInputField = element(by.name('dateOfBirthYear'));  
    this.emailInputField = element(by.css('#user-identifier-input'));
    this.passwordInputField = element(by.css('#password-input'));
    this.locationField = element(by.css('#location-select'));
    this.ITEM_IN_DROPDOWN = '//option[text()="{0}"]';
    this.successMessage = element(by.css('.header__title--success'));
    this.warningMessage = element.all(by.css('p.text')).first();
  }

  selectLocation(value){
    let locationName = element(by.xpath(format(this.ITEM_IN_DROPDOWN, value)));
    return locationField.click().then(() => {
      return locationName.click();
    })
  }
}

module.exports = RegistrationPage;