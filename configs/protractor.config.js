"use strict";

exports.config = {
    directConnect: true,    
    baseUrl: 'https://www.bbc.com/',
    capabilities: {
        browserName: 'chrome',
        'maxInstances': 1,
        chromeOptions: {
            args: [
                '--start-maximized',
                'disable-extensions',
                '--disable-infobars'
              ]
        },
        platform: "Windows 10",
        maxDuration: 10800
    },
    specs: [
       '../features/*.feature'
    ],    
    cucumberOpts: {
      require: ['../step_definitions/*.js'],
      tags: ['@smoke']
    }
};