'use strict';

const { When } = require('cucumber'),
  fakerator = require('fakerator')(),
  BasicPage = require('../page_objects/pages/basicPage'),
  LoginPage = require('../page_objects/pages/loginPage'),
  RegistrationPage = require('../page_objects/pages/registrationPage'),
  Helper = require('../utils/helper');

const basicPage = new BasicPage(),
  loginPage = new LoginPage(),
  registrationPage = new RegistrationPage(),
  helper = new Helper();


When(/^Click "([^"]*)" button$/, function (buttonName) {
  return basicPage.clickButton(buttonName);
});

When(/^Type value in "([^"]*)" field$/, function (fieldName) {
  let email = fakerator.internet.email(),
    password = fakerator.internet.password(9),
    day = fakerator.random.number(1, 28),
    month = fakerator.random.number(1, 12),
    year = fakerator.random.number(1940, 2000);

  let fieldNameMap = new Map([
    ['EMAIL', [email, registrationPage.emailInputField]],
    ['PASSWORD', [password, registrationPage.passwordInputField]],
    ['DAY OF BIRTHDAY', [day, registrationPage.dayInputField]],
    ['MONTH OF BIRTHDAY', [month, registrationPage.monthInputField]],
    ['YEAR OF BIRTHDAY', [year, registrationPage.yearInputField]],
  ]);

  return helper.scrollAndSendKeys(fieldNameMap.get(fieldName)[1], fieldNameMap.get(fieldName)[0]);
});

When(/^Select "([^"]*)" in "([^"]*)" field$/, function (countryName, fieldName) {
  return registrationPage.selectLocation(countryName);
});

When(/^Enter "([^"]*)" in "([^"]*)" field$/, function (value, fieldName) {
  let fieldNameMap = new Map([
    ['USERNAME', loginPage.userNameField],
    ['PASSWORD', loginPage.passwordField],
    ['DAY OF BIRTHDAY', registrationPage.dayInputField],
    ['MONTH OF BIRTHDAY', registrationPage.monthInputField],
    ['YEAR OF BIRTHDAY', registrationPage.yearInputField],
  ]);

  return helper.scrollAndSendKeys(fieldNameMap.get(fieldName), value);
});

When(/^Open "([^"]*)" form$/, function (buttonName) {
  let formMap = new Map([
    ['LOGIN', 'https://account.bbc.com/signin'],
    ['REGISTRATION', 'https://account.bbc.com/register'],
  ])
  return browser.get(formMap.get(buttonName));
});