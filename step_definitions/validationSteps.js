'use strict';

const { Then } = require('cucumber'),
  format = require('string-format'),
  BasicPage = require('../page_objects/pages/basicPage'),
  RegistrationPage = require('../page_objects/pages/registrationPage'),
  messageJson = require('../data/message');

const basicPage = new BasicPage(),
  registrationPage = new RegistrationPage();

Then(/^Check modal window is present$/, function () {
    return basicPage.modalWindow.isPresent().should.eventually.equal(true, messageJson.data.MODAL_WINDOW_IS_NOT_PRESENT);
});

Then(/^Check message "([^"]*)" is present$/, function (text) {
  return registrationPage.warningMessage.getText().should.eventually.equal(text, format(messageJson.data.MESSAGE_IS_NOT_PRESENT, text))
});

Then(/^Check "([^"]*)" page is present$/, function (pageName) {
  let pageNameMap = new Map ([
    ['HOME', 'https://www.bbc.com/'],    
  ]);

  return browser.getCurrentUrl().should.eventually.equal(pageNameMap.get(pageName), format(messageJson.data.PAGE_IS_NOT_PRESENT, pageName));
});