'use strict';

const { element } = require('protractor');

class BasicPage {

  constructor(){
    this.modalWindow = element(by.css('div.page__content-wrapper'));
  }

  clickButton(buttonName){
    return element(by.buttonText(buttonName)).click();
  }
    
}

module.exports = BasicPage;