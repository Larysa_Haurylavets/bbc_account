'use strict';

const { element } = require('protractor'),
  BasicPage = require('./basicPage');

class LoginPage extends BasicPage{
  constructor(){
    this.userNameField = element(by.css('#username'));
    this.passwordField = element(by.css('#password'));
    this.signInButton = element(by.css('#submit-button'));
  }
  

}

module.exports = LoginPage;