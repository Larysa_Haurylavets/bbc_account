Feature: Login Form

  As a user I will be able to login to account

  Scenario: Login with valid username and password
    When Open "LOGIN" form
    Then Check modal window is present
    When Enter "master_acc_test" in "USERNAME" field
    And Enter "1111test" in "PASSWORD" field
    And Click "Sign in" button